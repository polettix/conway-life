# life - a homage to John Horton Conway

On April 11, 2020 the [John Horton Conway][] the Great passed away due to
complicances related to Covid-19 ("*Coronavirus*").

Although he had a *peculiar* relation with his famous [Game of Life][] (e.g.
see [this video][conway-life]), I thought it nice to pay homage to the genius
with this humble contribution.

See [Installing Perl Modules][] to know what to do with `cpanfile`. After you
have modules in place, you can run the program like this:

```shell
$ perl -I local/lib/perl5 life
```

You can optionally pass an integer number to set the pause interval between
adjacent *ticks* of life.

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

[John Horton Conway]: https://en.wikipedia.org/wiki/John_Horton_Conway
[Game of Life]: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
[conway-life]: https://www.youtube.com/watch?v=E8kUJL04ELA
[Installing Perl Modules]: https://github.polettix.it/ETOOBUSY/2020/01/04/installing-perl-modules/
